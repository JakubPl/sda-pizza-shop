package pl.sda.pizza;

import pl.sda.pizza.exception.InvalidUserInputException;
import pl.sda.pizza.service.CookingService;
import pl.sda.pizza.service.DeliveryService;
import pl.sda.pizza.service.OrderService;
import pl.sda.pizza.service.ReportService;

import java.util.Scanner;

public class Runner {

    private static final String ORDER = "zamow";
    private static final String REPORT = "raport";
    private static final String COOK = "gotuj";

    public void run() {
        final Scanner scanner = new Scanner(System.in);
        final OrderService orderService = new OrderService();
        final ReportService reportService = new ReportService();
        final CookingService cookingService = new CookingService(orderService);
        final DeliveryService deliveryService = new DeliveryService(reportService);

        String choice = null;
        while (!"q".equals(choice)) {
            //kiedy 5 pizz jest gotowych powinna zostac wyslana ciezarowka i zaraportowane pizze
            choice = scanner.nextLine();
            switch (choice) {
                case ORDER:
                    try {
                        final String order = scanner.nextLine();
                        orderService.takeOrder(order);
                    } catch (InvalidUserInputException e) {
                        System.out.println("Nie posiadamy wybranych skladnikow");
                    }
                    //tu obsluga zamawiania
                    break;
                case REPORT:
                    reportService.printReport();
                    //tu obsluga tworzenia generowania raportu
                    break;
                case COOK:
                    cookingService.cookNext();
                    //tu obsluga gotowania
                    break;

            }
        }
    }
}
