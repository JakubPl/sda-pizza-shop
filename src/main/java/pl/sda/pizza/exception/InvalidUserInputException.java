package pl.sda.pizza.exception;

public class InvalidUserInputException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Wprowadzony tekst nie zawiera skladnikow";
    }
}
