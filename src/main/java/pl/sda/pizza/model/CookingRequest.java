package pl.sda.pizza.model;

import java.util.List;

public class CookingRequest {
    private List<Ingredient> ingredients;

    public CookingRequest(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }
}
