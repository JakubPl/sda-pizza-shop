package pl.sda.pizza.model;

public enum Ingredient {
    //uzupelnij!
    CHEESE(""), SALAMI(""), OLIVES(""), ONION("");

    private final String ingredientReadableName;

    Ingredient(String ingredientReadableName) {

        this.ingredientReadableName = ingredientReadableName;
    }

    public String getIngredientReadableName() {
        return ingredientReadableName;
    }
}
