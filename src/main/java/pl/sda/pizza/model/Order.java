package pl.sda.pizza.model;

import java.util.List;

public class Order {
    private List<Ingredient> ingredients;
    private String address;

    public Order(List<Ingredient> ingredients, String address) {
        this.ingredients = ingredients;
        this.address = address;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public String getAddress() {
        return address;
    }
}
