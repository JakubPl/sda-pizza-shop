package pl.sda.pizza.model;

import java.util.List;

public class Pizza {
    private List<Ingredient> ingredients;
    private Quality quality;
    public Pizza(List<Ingredient> ingredients, Quality quality) {
        this.ingredients = ingredients;
        this.quality = quality;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public Quality getQuality() {
        return quality;
    }
}
