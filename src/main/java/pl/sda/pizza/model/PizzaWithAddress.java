package pl.sda.pizza.model;

public class PizzaWithAddress {
    private Pizza pizza;
    private String address;

    public PizzaWithAddress(Pizza pizza, String address) {
        this.pizza = pizza;
        this.address = address;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public String getAddress() {
        return address;
    }
}
