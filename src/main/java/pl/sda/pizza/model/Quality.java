package pl.sda.pizza.model;

public enum Quality {
    WORST(0), MEDIUM(5), BEST(10);

    private final int qualityNumber;

    Quality(int qualityNumber) {

        this.qualityNumber = qualityNumber;
    }
}
