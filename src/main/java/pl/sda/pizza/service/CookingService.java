package pl.sda.pizza.service;

import pl.sda.pizza.model.CookingRequest;
import pl.sda.pizza.model.Pizza;

public class CookingService {
    private final OrderService orderService;

    public CookingService(OrderService orderService) {
        this.orderService = orderService;
    }

    public Pizza cookNext() {
        final CookingRequest nextOrder = orderService.viewTopCookingRequest();
        return null;
    }
}
