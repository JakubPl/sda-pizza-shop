package pl.sda.pizza.service;

import pl.sda.pizza.model.CookingRequest;
import pl.sda.pizza.model.Order;
import pl.sda.pizza.model.Pizza;
import pl.sda.pizza.model.PizzaWithAddress;

import java.util.Queue;

public class OrderService {
    private Queue<Order> orders;
    private Queue<PizzaWithAddress> readyOrders;

    public int readyPizzasCount() {
        return readyOrders.size();
    }

    public void takeOrder(String order) {

    }

    public void markPizzaAsReady(Pizza pizza) {

    }

    public CookingRequest viewTopCookingRequest() {
        return null;
    }
}
